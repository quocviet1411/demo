package com.example.quocviet.material_design;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.quocviet.material_design.pojo.Information;

import java.util.Collections;
import java.util.List;

/**
 * Created by quocviet on 29/08/2015.
 */
public class Adapter_Recycyview extends RecyclerView.Adapter {
    private LayoutInflater layoutInflater;
    TextView title;
    ImageView icon;
    Context c;
    List<Information> data = Collections.emptyList();
    public Adapter_Recycyview(Context context, List<Information> dt){
        layoutInflater = LayoutInflater.from(context);
        this.data = dt;
        this.c = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.custom_row,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Information curent = data.get(position);
       title.setText(curent.title);
       icon.setImageResource(curent.iconid);
    }

    @Override
    public int getItemCount() {
        return 0;
    }
    class MyViewHolder extends RecyclerView.ViewHolder{

        public MyViewHolder(View itemView) {
            super(itemView);

            title = (TextView)itemView.findViewById(R.id.listTest);
            icon = (ImageView)itemView.findViewById(R.id.listIcon);
        }
    }
}
