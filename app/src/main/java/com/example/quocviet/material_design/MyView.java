package com.example.quocviet.material_design;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

/**
 * Created by quocviet on 14/09/2015.
 */
public class MyView extends TextView {
    public static final String TAG="VIVZ";
    Paint paint = null;
    public MyView(Context context) {
        super(context);
        init();
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setAntiAlias(true);
        setWillNotDraw(false);
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getActionMasked())
        {
            case MotionEvent.ACTION_DOWN:
                Log.d(TAG, "Myview dispatchTouchEven Down");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d(TAG, "Myview dispatchTouchEven MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.d(TAG, "Myview dispatchTouchEven UP");
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.d(TAG, "Myview dispatchTouchEven CANCEL");
                break;
        }
        boolean b= super.dispatchTouchEvent(ev);
        Log.d(TAG, "Myview dispatchTouchEvent" + b);
        return b;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked())
        {
            case MotionEvent.ACTION_DOWN:
                Log.d(TAG,"Myview onTouchEvent Down");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d(TAG, "Myview onTouchEvent MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.d(TAG, "Myview onTouchEvent UP");
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.d(TAG, "Myview onTouchEvent CANCEL");
                break;
        }
      //  boolean b = super.onTouchEvent(event);
       // Log.d(TAG,"Myview onTouchEvent"+b);
        return true;
    }

}
