package com.example.quocviet.material_design;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.quocviet.material_design.pojo.Information;

import java.util.Collections;
import java.util.List;

/**
 * Created by quocviet on 29/08/2015.
 */
public class RecycviewAdapter extends RecyclerView.Adapter<RecycviewAdapter.Myholder> {
    private LayoutInflater layoutInflater;
    Context c;
    List<Information>data = Collections.emptyList();
    public Clicklistener clicklistener;
    public RecycviewAdapter(Context context, List<Information> dt){
        layoutInflater = LayoutInflater.from(context);
        this.data = dt;
        this.c = context;
    }
    public void remove(int position)
    {
        data.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = layoutInflater.inflate(R.layout.custom_row,parent,false);
        Myholder holder = new Myholder(root);
        Log.d("VIVZ", "Creat holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(Myholder holder,int position) {
        Information curent = data.get(position);
        holder.title.setText(curent.title);
        holder.icon.setImageResource(curent.iconid);
        Log.d("VIVZ", "position" + position);
      /*  holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(c,"Item Click at: "+position,Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    public void setClicklistener(Clicklistener clicklistener)
    {
        this.clicklistener = clicklistener;
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public  class Myholder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView title;
        ImageView icon;
        public Myholder(View itemView) {
            super(itemView);
            title= (TextView) itemView.findViewById(R.id.listTest);
            icon = (ImageView) itemView.findViewById(R.id.listIcon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
          //  remove(getPosition());
          //  c.startActivity(new Intent(c,SubActivity.class));
            if(clicklistener!=null)
            {
                clicklistener.itemClicked(v,getPosition());
            }
        }
    }
    public interface Clicklistener{
        public void itemClicked(View view,int position);
    }
}
