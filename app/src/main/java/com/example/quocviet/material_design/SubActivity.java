package com.example.quocviet.material_design;


import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

public class SubActivity extends AppCompatActivity {
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==android.R.id.home) {

            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getActionMasked())
        {
            case MotionEvent.ACTION_DOWN:
                Log.e("", "Activity dispatchTouchEven Down");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.e("", "Activity dispatchTouchEven MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.e("", "Activity dispatchTouchEven UP");
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.e("", "Activity dispatchTouchEven CANCEL");
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked())
        {
            case MotionEvent.ACTION_DOWN:
                Log.e("","Activity Touchevent Down");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.e("", "Activity Touchevent MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.e("", "Activity Touchevent UP");
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.e("", "Activity Touchevent CANCEL");
                break;
        }
        return super.onTouchEvent(event);
    }
}
