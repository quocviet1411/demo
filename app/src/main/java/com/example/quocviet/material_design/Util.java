package com.example.quocviet.material_design;

import android.os.Build;

/**
 * Created by quocviet on 28/09/2015.
 */
public class Util {
    public static boolean isLollipopOrGreater(){
        return Build.VERSION.SDK_INT>=21?true:false;
    }
    public static boolean isJellyBeanOrGreater(){
        return Build.VERSION.SDK_INT>=16?true:false;
    }
}
