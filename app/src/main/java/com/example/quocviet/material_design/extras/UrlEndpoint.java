package com.example.quocviet.material_design.extras;

/**
 * Created by quocviet on 29/09/2015.
 */
public class UrlEndpoint {
    public static final String URL_BOX_OFFICE="http://api.rottentomatoes.com/api/public/v1.0/lists/movies/box_office.json";
    public static final String URL_CHAR_QUESTION="?";
    public static final String URL_CHAR_AMEPERSAND="&";
    public static final String URL_PARAM_APIKEY="apikey=";
    public static final String URL_PARAM_LIMIT="limit=";
}
