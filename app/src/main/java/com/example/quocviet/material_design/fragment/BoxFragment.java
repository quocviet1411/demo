package com.example.quocviet.material_design.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.quocviet.material_design.R;
import com.example.quocviet.material_design.extras.UrlEndpoint;
import com.example.quocviet.material_design.logging.L;
import com.example.quocviet.material_design.network.MyApplication;
import com.example.quocviet.material_design.network.VolleySingleton;
import com.example.quocviet.material_design.pojo.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_AUDIENCE_SCORE;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_ID;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_MOVIES;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_POSTERS;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_RATINGS;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_RELEASE_DATES;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_SYNOPSIS;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_THEATER;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_THUMBNAIL;
import static com.example.quocviet.material_design.extras.Key.EndpointBoxOffice.KEY_TITLE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BoxFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BoxFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String URL_ROTTEN_TOMATOES_BOX_OFFICE="http://api.rottentomatoes.com/api/public/v1.0/lists/movies/box_office.json";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private RequestQueue requestQueue;
    private ArrayList<Movie> listMovies = new ArrayList<>();
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BoxFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BoxFragment newInstance(String param1, String param2) {
        BoxFragment fragment = new BoxFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public static String getRequestUrl(int limit){
        return UrlEndpoint.URL_BOX_OFFICE+UrlEndpoint.URL_CHAR_QUESTION+UrlEndpoint.URL_PARAM_APIKEY
                +MyApplication.API_KEY_ROTTEN_TOMATOES+UrlEndpoint.URL_CHAR_AMEPERSAND+UrlEndpoint.URL_PARAM_LIMIT+limit;
    }
    public BoxFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        sendJsonRequest();
    }
    private void sendJsonRequest(){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                getRequestUrl(10),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsonResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(request);
    }
    private void parseJsonResponse(JSONObject response) {
            if(response==null|| response.length()==0){
                return;
            }
            try {
                StringBuilder data = new StringBuilder();
                JSONArray arrayMovies = response.getJSONArray(KEY_MOVIES);
                for (int i=0;i<arrayMovies.length();i++){
                    JSONObject currentMovies = arrayMovies.getJSONObject(i);
                    long id = currentMovies.getLong(KEY_ID);
                    String title = currentMovies.getString(KEY_TITLE);
                    JSONObject objectReleaseDates = currentMovies.getJSONObject(KEY_RELEASE_DATES);
                    String releaseDate=null;
                    if(objectReleaseDates.has(KEY_THEATER)){
                        releaseDate=objectReleaseDates.getString(KEY_THEATER);
                    }
                    else{
                        releaseDate="N/A";
                    }
                    //get the audicen score for the curent movie
                    JSONObject objectRating = currentMovies.getJSONObject(KEY_RATINGS);
                    int audienceScore=-1;
                    if(objectRating.has(KEY_AUDIENCE_SCORE))
                    {
                        audienceScore=objectRating.getInt(KEY_AUDIENCE_SCORE);
                    }
                    //get the synopsis of the current movies
                    String synopsis = currentMovies.getString(KEY_SYNOPSIS);
                    JSONObject objectPoster = currentMovies.getJSONObject(KEY_POSTERS);
                    String urlThumbnail = null;
                    if(objectPoster.has(KEY_THUMBNAIL)){
                        urlThumbnail=objectPoster.getString(KEY_THUMBNAIL);
                    }
                    Movie movie = new Movie();
                    movie.setId(id);
                    movie.setTitle(title);
                    try {
                        Date date = dateFormat.parse(releaseDate);
                        movie.setReleaseDateTheater(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    movie.setAudienceScore(audienceScore);
                    movie.setSysnopsis(synopsis);
                    movie.setUrlThumbnail(urlThumbnail);
                    listMovies.add(movie);
                }
                L.t(getActivity(),listMovies.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_box, container, false);
    }


}
