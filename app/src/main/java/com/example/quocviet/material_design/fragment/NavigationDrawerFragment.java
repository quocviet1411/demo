package com.example.quocviet.material_design.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.quocviet.material_design.Adapter_Recycyview;
import com.example.quocviet.material_design.pojo.Information;
import com.example.quocviet.material_design.R;
import com.example.quocviet.material_design.RecycviewAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment /*implements RecycviewAdapter.Clicklistener*/{
    private RecyclerView recyclerView;
    public static final String PREF_FILE_NAME="testpref";
    public static final String KEY_USER_LEARNED_DRAWER="user_learned_drawer";
    private static DrawerLayout mdrawrelayout;
    private static ActionBarDrawerToggle mdrawertogether;
    private boolean mUserLeanredDrawer;
    private boolean mFromSaveinstanceState;
    private View contentview;
    private Toolbar toolbar;
    private RecycviewAdapter recycviewAdapter;
    private Adapter_Recycyview adapter_recycyview;
    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLeanredDrawer=Boolean.valueOf(readFromPrefrence(getActivity(),KEY_USER_LEARNED_DRAWER,"false"));
        if(savedInstanceState!=null)
        {
            mFromSaveinstanceState=true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView)layout.findViewById(R.id.drawerlist);
        recycviewAdapter = new RecycviewAdapter(getActivity(),getdata());
       // recycviewAdapter.setClicklistener(this);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(getActivity(),"onClick"+position,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(getActivity(),"onlongClick"+position,Toast.LENGTH_SHORT).show();
            }
        }));
        recyclerView.setAdapter(recycviewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return layout;
    }

    public static List<Information> getdata()
    {
        List<Information>data = new ArrayList<Information>();
        int[] icons = {R.drawable.time,R.drawable.title,R.drawable.alert,R.drawable.gift};
        String[] titles = {"Google","Yuotube","Zing","Mp3"};
        for(int i=0;i<100;i++)
        {
            Information current = new Information();
            current.iconid=icons[i%icons.length];
            current.title=titles[i%titles.length];
            data.add(current);
        }
        return data;
    }

    public void setUp(int fragmentid,DrawerLayout layout, final Toolbar tool) {
        toolbar = tool;
        contentview = getActivity().findViewById(fragmentid);
        mdrawrelayout = layout;
        mdrawertogether = new ActionBarDrawerToggle(getActivity(),layout,tool,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
              if(!mUserLeanredDrawer)
              {
                  mUserLeanredDrawer=true;
                  saveToPrefrence(getActivity(),KEY_USER_LEARNED_DRAWER,mUserLeanredDrawer+"");
              }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if(slideOffset<0.6)
                {
                   toolbar.setAlpha(1-slideOffset);
                }
               // Log.e("KQ ","Offset"+slideOffset);
            }
        };
        if(!mUserLeanredDrawer&& !mFromSaveinstanceState){
            mdrawrelayout.openDrawer(contentview);
        }
        mdrawrelayout.setDrawerListener(mdrawertogether);
        mdrawrelayout.post(new Runnable() {
            @Override
            public void run() {
                mdrawertogether.syncState();
            }
        });
    }
    public static void saveToPrefrence(Context context,String prefren_name,String prefren_value)
    {
        SharedPreferences sharepref = context.getSharedPreferences(PREF_FILE_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharepref.edit();
        editor.putString(prefren_name,prefren_value);
        editor.apply();
    }
    public static String readFromPrefrence(Context context, String prefren_name, String prefren_value)
    {
        SharedPreferences sharepref = context.getSharedPreferences(PREF_FILE_NAME,Context.MODE_PRIVATE);
        return sharepref.getString(prefren_name,prefren_value);
    }

   /* @Override
    public void itemClicked(View view, int position) {
        startActivity(new Intent(getActivity(),SubActivity.class));
    }*/
    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{
       private GestureDetector gestureDetector;
       private ClickListener clickListener;
        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener){
            Log.d("quocviet","khoi tao");
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    Log.d("quocviet","onSingleTapup"+e);
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null&&clickListener!=null)
                    {
                        clickListener.onLongClick(child,recyclerView.getChildLayoutPosition(child));
                    }
                    Log.d("quocviet","onLongPress"+e);
                }
            });

        }
       @Override
       public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
           Log.d("quocviet","onInterceptTouchEvent"+e);
           View child = rv.findChildViewUnder(e.getX(),e.getY());
           if(child!=null&&clickListener!=null&&gestureDetector.onTouchEvent(e));
           {
               clickListener.onClick(child,rv.getChildLayoutPosition(child));
           }
           return false;
       }

       @Override
       public void onTouchEvent(RecyclerView rv, MotionEvent e) {
           Log.d("quocviet","onTouchEvent"+e);
       }

       @Override
       public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

       }
   }
    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }
}
