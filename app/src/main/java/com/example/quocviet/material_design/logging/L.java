package com.example.quocviet.material_design.logging;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by quocviet on 29/09/2015.
 */
public class L {
    public static void m(String message){
        Log.d("QV"," "+message);
    }
    public static void t(Context context, String message){
        Toast.makeText(context,message+"",Toast.LENGTH_SHORT).show();
    }
}
