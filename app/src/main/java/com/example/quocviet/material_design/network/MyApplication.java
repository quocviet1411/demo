package com.example.quocviet.material_design.network;

import android.app.Application;
import android.content.Context;

/**
 * Created by quocviet on 25/09/2015.
 */
public class MyApplication extends Application {
    public static final String API_KEY_ROTTEN_TOMATOES="7ue5rxaj9xn4mhbmsuexug54";
    private static MyApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance=this;
    }
    public static MyApplication getsInstance(){
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }
}
