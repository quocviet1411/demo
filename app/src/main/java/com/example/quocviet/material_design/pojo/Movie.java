package com.example.quocviet.material_design.pojo;

import java.util.Date;

/**
 * Created by quocviet on 29/09/2015.
 */
public class Movie {
    private Long id;
    private String title;
    private Date releaseDateTheater;
    private int audienceScore;
    private String sysnopsis;
    private String urlThumbnail;
    private String urlSelf;
    private String urlCast;
    private String urlReviews;
    private String urlSimilar;
    public Movie(){}
    public Movie(Long id,String title,
        Date releaseDateTheater,
        int audienceScore,
        String sysnopsis,
        String urlThumbnail,
        String urlSelf,
        String urlCast,
        String urlReviews,
        String urlSimilar){
        this.id = id;
        this.title=title;
        this.releaseDateTheater = releaseDateTheater;
        this.audienceScore = audienceScore;
        this.sysnopsis = sysnopsis;
        this.urlThumbnail=urlThumbnail;
        this.urlSelf = urlSelf;
        this.urlCast = urlCast;
        this.urlReviews =urlReviews;
        this.urlSimilar = urlSimilar;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setReleaseDateTheater(Date releaseDateTheater) {
        this.releaseDateTheater = releaseDateTheater;
    }

    public Date getReleaseDateTheater() {
        return releaseDateTheater;
    }

    public void setAudienceScore(int audienceScore) {
        this.audienceScore = audienceScore;
    }

    public int getAudienceScore() {
        return audienceScore;
    }

    public void setSysnopsis(String sysnopsis) {
        this.sysnopsis = sysnopsis;
    }

    public String getSysnopsis() {
        return sysnopsis;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlSelf(String urlSelf) {
        this.urlSelf = urlSelf;
    }

    public String getUrlSelf() {
        return urlSelf;
    }

    public void setUrlCast(String urlCast) {
        this.urlCast = urlCast;
    }

    public String getUrlCast() {
        return urlCast;
    }

    public void setUrlReviews(String urlReviews) {
        this.urlReviews = urlReviews;
    }

    public String getUrlReviews() {
        return urlReviews;
    }

    public void setUrlSimilar(String urlSimilar) {
        this.urlSimilar = urlSimilar;
    }

    public String getUrlSimilar() {
        return urlSimilar;
    }

    @Override
    public String toString() {
        return "ID:"+id+"Title"+title+"Date"+releaseDateTheater+"Synopsis"+sysnopsis+"Score"+audienceScore
                +"urlThumbnail"+urlThumbnail;
    }
}
